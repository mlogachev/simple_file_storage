import json
import hashlib


def test_file_that_doesnt_exist(client):
    hash_that_doesnt_exist = 'abc'
    resp = client.get(f'/file/{hash_that_doesnt_exist}')
    assert resp.status_code == 404


def test_random_file(client, random_binary_file):
    f_hash = hashlib.sha256(random_binary_file).hexdigest()

    resp = client.get(f'/file/{f_hash}')
    assert resp.status_code == 404


def test_file_get(client, random_binary_file):
    resp = client.post('/file', data=random_binary_file)
    f_hash = json.loads(resp.data)['file_hash']
    assert f_hash == hashlib.sha256(random_binary_file).hexdigest()

    resp = client.get(f'/file/{f_hash}')
    assert resp.status_code == 200
    assert resp.data == random_binary_file


def test_image_get(client, random_binary_file):
    resp = client.post('/file', data=random_binary_file)
    f_hash = json.loads(resp.data)['file_hash']
    assert f_hash == hashlib.sha256(random_binary_file).hexdigest()

    resp = client.get(f'/file/{f_hash}?content_type=image/png')
    assert resp.status_code == 200
    assert resp.content_type == 'image/png'
    assert resp.data == random_binary_file
