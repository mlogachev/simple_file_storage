import hashlib
import json
import pytest
import os


def test_index(client):
    response = client.get('/')
    assert response.status_code == 200
    assert response.data.decode('utf-8') == 'Hello World!'


def test_file_uploading(client, random_binary_file):
    file_hash = hashlib.sha256(random_binary_file).hexdigest()
    response = client.post('/file', data=random_binary_file)

    assert response.status_code == 200
    assert json.loads(response.data)['file_hash'] == file_hash


def test_uploading_empty_file(client):
    binary_file = b''

    resp = client.post('/file', data=binary_file)
    assert resp.status_code == 400


@pytest.mark.parametrize(
    'local_file',
    [os.path.join(os.path.dirname(__file__), 'test_data', 'sample_file_1')],
    indirect=True
)
def test_concrete_file_uploading_1(client, local_file):
    file_hash = hashlib.sha256(local_file).hexdigest()
    response = client.post('/file', data=local_file)

    assert response.status_code == 200
    assert json.loads(response.data)['file_hash'] == file_hash

    response = client.post('/file', data=local_file)
    assert response.status_code == 409
    assert json.loads(response.data)['file_hash'] == file_hash
