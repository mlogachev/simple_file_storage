import random
import pytest
import os
import shutil

from app import app

from config import CONFIG


@pytest.fixture(scope='session', autouse=True)
def setup_file_structure(request):

    if 'data' not in os.listdir(os.path.join(os.path.dirname(__file__), 'test_data')):
        os.mkdir(os.path.join(os.path.dirname(__file__), 'test_data', 'data'))

    def clean_up():
        dirs = [
            os.path.join(os.path.dirname(__file__), 'test_data', 'data', f)
            for f in os.listdir(os.path.join(os.path.dirname(__file__), 'test_data', 'data'))
            if os.path.isdir(os.path.join(os.path.dirname(__file__), 'test_data', 'data', f))
        ]
        for directory in dirs:
            shutil.rmtree(directory)

    request.addfinalizer(clean_up)


@pytest.fixture(scope='function')
def client():
    app.config['TESTING'] = True
    app.config['DATA_DIR'] = CONFIG['TEST_DATA_DIR']
    return app.test_client()


@pytest.fixture(scope='function')
def random_binary_file():
    file_size = random.randint(0, 1100)
    random_binary = bytes([random.getrandbits(8) for _ in range(file_size)])
    return random_binary


@pytest.fixture(scope='function')
def local_file(request):
    with open(request.param, 'rb') as f:
        return f.read()
