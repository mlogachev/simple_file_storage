import json
import hashlib


def test_deleting_file_that_doesnt_exist(client):
    impossible_f_hash = '123'
    resp = client.delete(f'/file/{impossible_f_hash}')

    assert resp.status_code == 404


def test_deleting_some_file(client, random_binary_file):
    f_hash = hashlib.sha256(random_binary_file).hexdigest()

    resp = client.get(f'/file/{f_hash}')
    assert resp.status_code == 404

    resp = client.post('/file', data=random_binary_file)
    assert resp.status_code == 200
    assert json.loads(resp.data)['file_hash'] == f_hash

    resp = client.get(f'/file/{f_hash}')
    assert resp.status_code == 200
    assert resp.data == random_binary_file

    resp = client.delete(f'/file/{f_hash}')
    assert resp.status_code == 200
    assert json.loads(resp.data)['success']

    resp = client.get(f'/file/{f_hash}')

    assert resp.status_code == 404
