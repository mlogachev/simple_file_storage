import itertools

from config import CONFIG
from os import listdir, mkdir
from os.path import dirname, join


if __name__ == '__main__':
    data_dir = join(dirname(dirname(__file__)), CONFIG['DATA_DIR'])
    symbols = ["{0:x}".format(val) for val in range(16)]
    for pair in itertools.product(symbols, repeat=2):
        folder_name = "{}{}".format(*pair)
        if folder_name not in listdir(data_dir):
            mkdir(join(data_dir, folder_name))
