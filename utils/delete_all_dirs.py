from config import CONFIG

from os import listdir
from os.path import dirname, join, isdir
from shutil import rmtree

if __name__ == '__main__':
    data_dir = join(dirname(dirname(__file__)), CONFIG['DATA_DIR'])
    for name in listdir(data_dir):
        dir_path = join(data_dir, name)
        if isdir(dir_path):
            rmtree(dir_path)
