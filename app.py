import hashlib
import os

from flask import Flask, request, jsonify, make_response

from config import CONFIG

app = Flask(__name__)
app.config['DATA_DIR'] = CONFIG['DATA_DIR']

if app.config['DATA_DIR'] not in os.listdir(os.path.dirname(__file__)):
    os.mkdir(os.path.join(os.path.dirname(__file__), app.config['DATA_DIR']))


@app.route('/')
def hello_world():
    return 'Hello World!'


@app.route('/file/<file_hash>', methods=['GET', 'DELETE'])
def get_file(file_hash):
    folder_name = file_hash[:2]
    data_dir = os.path.join(os.path.dirname(__file__), app.config['DATA_DIR'])

    if folder_name not in os.listdir(data_dir):
        return '', 404

    file_folder_path = os.path.join(data_dir, folder_name)
    if (
            file_hash not in os.listdir(file_folder_path) or
            not os.path.isfile(os.path.join(file_folder_path, file_hash))
    ):
        return '', 404

    if request.method == 'GET':
        with open(os.path.join(file_folder_path, file_hash), 'rb') as file:
            content_type = request.args.get('content_type', 'text/plain')
            resp = make_response(file.read())
            resp.headers.set('Content-Type', content_type)
            return resp
    else:
        os.remove(os.path.join(file_folder_path, file_hash))
        return jsonify({"success": True}), 200


@app.route('/file', methods=['POST'])
def put_file():
    binary_file = request.data

    if not binary_file:
        return jsonify({"success": False, "message": "File data is missing"}), 400

    file_hash = hashlib.sha256(binary_file).hexdigest()
    folder_name = file_hash[:2]

    data_dir = os.path.join(os.path.dirname(__file__), app.config['DATA_DIR'])

    if folder_name not in os.listdir(data_dir):
        os.mkdir(os.path.join(data_dir, folder_name))

    folder_path = os.path.join(data_dir, folder_name)

    if file_hash in os.listdir(folder_path):
        return (
            jsonify({"success": False, "message": f"File with such hash already exists", "file_hash": file_hash}),
            409
        )

    with open(os.path.join(folder_path, file_hash), 'wb') as file:
        file.write(binary_file)

    return jsonify({"success": True, "message": "Success", "file_hash": file_hash}), 200


if __name__ == '__main__':
    app.run(debug=True)
