# Простое хранилище данных в файловой системе

## Запуск
```
cd /path/to/clone/simple_file_storage
python3 -m venv venv/
. venv/bin/activate
pip install -r requirement.txt
export FLASK_APP=app.py
flask run
``` 

## Описание методов

- GET /file/{file_hash}?content_type={content_type}
    
    Получить файл по заданному хэшу
    
    Параметры: 
    - file_hash: str (обязательный) - хэш файла, полученный при его загрузке
    - content_type: str - желаемый content_type файла, по умолчанию: text/plain
    
    Формат ответа:
    - 200 - файл найден, ответ в body ответа с переданным content_type
    - 404 - файл не найден, пустой ответ
    
- POST /file/
    
    Загрузить файл в хранилище
    
    Параметры:
    body - файл передается в body запроса
    
    Формат ответа:
    - 200 - OK, в ответе json вида, с хэш-значением полученного файла
    ```
    {"success": true, "message": "Success", "file_hash": "{file_hash}"} 
    ```
    - 400 - Файл пустой или ничего не передано в тело запроса, json вида
    ```
    {"success": false, "message": "File data is missing"}
    ```
    - 409 - Конфликт хэш значения с уже имеющимся файлом, json вида
    ```
    {"success": false, "message": "File with such hash already exists", "file_hash": {file_hash}}
    ```
    
- DELETE /file/{file_hash}
    
    Удалить файл из хранильща по заданному ключу
    
    Параметры:
    - file_hash: str (обязательный) - хэш файла, полученный при его загрузке
    
    Формат ответа:
    - 200 - Файл успешно удален
    ```
    {"success": true}
    ```
    - 404 - Файл не найден по заданному хэшу